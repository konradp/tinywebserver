#include <fstream>
#include "Support.h"
// local
#include <tinyhttpparser/src/HttpParser.hpp>
#include <tinysocketlib/src/Tinysocketlib.hpp>
#include "Parser.hpp"

using namespace Tinysocketlib;
class Webserver : public Server {
public:
    void Handler(int file_desc) {
        std::cout << "Handling incoming connection" << std::endl;
        int nbytes;
        char buffer[1024];
    
        int bytesIn = 0;

        // Receive data
        if((bytesIn = recv(file_desc, buffer, sizeof(buffer), 0)) > 0) {
            // Parse request
            HttpMsg* http_msg = new HttpMsg;
            Parser http_parser;
            http_parser.SetBuf(buffer, bytesIn-1);
            http_parser.SetHttpMsg(http_msg);
            http_parser.Parse();

            // Get URL and split it
            std::string url = http_msg->GetUrl();
            std::cout << "URL: " << url << std::endl;
            try {
                std::string path = PreparePath(url);
                std::ifstream ifs(path, std::ios::in);
                std::string line, key, value;

                if(ifs.is_open()) {
                    int bufsize = 2;
                    char * buf = new char[bufsize];
                    while(ifs.read(buf, bufsize)) {
                        SendBuf(file_desc, buf, bufsize);
                    }
                }
            } catch(const std::exception& e) {
                std::cout << "Exception: " << e.what() << std::endl;
                // TODO: Send HTTP 404
            }

            // Close connection
            close(file_desc);
        } // recv
    }; // Handler

    // Prepare the path from url
    // TODO: Safeguard against paths like ../../../etc/passwd
    // Use index.html if dir is given
    // etc
    std::string PreparePath(std::string url) {
        std::vector<std::string> v = Support::StringSplit("/", url);
        for(auto i : v) {
            // TODO: Safeguard against ../../../etc/passwd here
            //std::cout << i << std::endl;
        }
        // REMOVE

        // Use document root path
        std::string path = _document_root + url;

        // Cycle index.html index.htm files if dir path specified
        if(Support::IsDir(path)) {
            // We have a dir
            bool found = false;
            for(auto i : _index_list) {
                // Cycle available index files
                if(Support::IsFile(path + "/" + i)) {
                    path = path + "/" + i;
                    found = true;
                    break;
                }
            }
            if(!found) throw std::runtime_error("No index.html in " + path);
        }

        return path;
    };
    
    void SetDocumentRoot(std::string r) { _document_root = r; };
    void SetIndexList(std::string s) {
        std::vector<std::string> v = Support::StringSplit(",", s);
        _index_list = v;
    };
    std::string GetIndexFile() { return _index_list[0]; };
    

private:
    std::string _document_root;
    std::vector<std::string> _index_list;

}; // class Webserver

