#include <iostream>
#include "Webserver.hpp"
#include "ConfigRead.hpp"
#include "Support.h"

int main()
{
    Webserver s;

    // Read config
    std::string cfgPath("./config.cfg");
    std::ifstream cfgFile(cfgPath);
    if(cfgFile && Support::IsFile(cfgPath)) {
        // If config exists, use it
        std::cout << "Using config file " << cfgPath << std::endl;
        try {
            ConfigRead c(cfgPath);
            s.SetPort(std::stoi(c.GetValue("port")));
            s.SetDocumentRoot(c.GetValue("document_root"));
            s.SetIndexList(c.GetValue("index_list"));
        } catch(const std::exception& e) {
            std::cout << "Failed reading config." << std::endl;
            std::cerr << e.what() << std::endl;
            exit(1);
        }
    } else {
        // No config file, use default values
        std::cout << "No config.cfg file detected. Using default settings." << std::endl;
        s.SetPort(80);
        s.SetDocumentRoot("./");
        s.SetIndexList("index.html");
    }

    // Start server
    s.Run();
}

