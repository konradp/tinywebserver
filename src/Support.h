#ifndef SUPPORT_H
#define SUPPORT_H

#include <string>
#include <sys/stat.h>
#include <vector>

namespace Support {
    std::vector<std::string>
    StringSplit(std::string delimiter, std::string text);

    bool IsFile(std::string path);
    bool IsDir(std::string path);
}; // namespace Support

#endif // SUPPORT_H

