#include <fstream>
#include "Support.h"

namespace Support {
    std::vector<std::string>
    StringSplit(std::string delimiter, std::string text)
    {
        std::vector<std::string> v;
        size_t m = text.find(delimiter);
        while(m != std::string::npos) {
    	v.push_back(text.substr(0, m)); // text until delimiter
    	text = text.substr(m + delimiter.size(), std::string::npos); // rest
    	m = text.find(delimiter); // update position
        }
        if(text.size() > 0) v.push_back(text);
        return v;
    };
    
    bool IsFile(std::string path) {
        struct stat path_stat;
        stat(path.c_str(), &path_stat);
        return S_ISREG(path_stat.st_mode);
    };
    
    bool IsDir(std::string path) {
        struct stat path_stat;
        stat(path.c_str(), &path_stat);
        return S_ISDIR(path_stat.st_mode);
    };

}; // namespace Support

