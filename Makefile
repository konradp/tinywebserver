OBJS = src/Support.cc src/main.cc
OBJ_NAME = tinywebserver
FLAGS = -w -std=c++17 -I lib/

all: $(OBJS)
	g++ $(OBJS) $(FLAGS) -o $(OBJ_NAME)

clean:
	rm tinywebserver

debug: $(OBJS)
	g++ $(OBJS) $(FLAGS) -g -o $(OBJ_NAME)

