# tinywebserver
A small c++ HTTP web server.

Note: Run as sudo to bind to port 80.

## Compile and run
Get the dependencies, compile, and run with
```
tpm
make
sudo ./tinywebserver
```

# Dependencies
This project uses:
- [tpm](https://gitlab.com/konradp/tpm): Packaging tool (reads `packages.json` and runs `git clone` for each repository
- [tinyhttpparser](https://gitlab.com/konradp/tinyhttpparser): HTTP parser
- [tinysocketlib](https://gitlab.com/konradp/tinysocketlib): wrapper for UNIX sockets
